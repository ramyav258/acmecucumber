Feature: Search for Vendor Name


Scenario Outline: Positive flow

Given Open the chrome browser
And launch the URL
And Enter the username as <uname>
And Enter the password as <pwd>
And Click on Login
And Click on Search Vendor
And Enter TaxID as <id>
And Click on Search Button
When List is displayed
Then Get the Vendor Name

Examples:
|uname|pwd|id|
|ramya.v258@gmail.com|ramya@123|IT145632|