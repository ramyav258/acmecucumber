package steps;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VendorName {
	
	public static ChromeDriver driver;

	@Given("Open the chrome browser")
	public void openTheChromeBrowser() {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
	}

	@Given("launch the URL")
	public void launchTheURL() {
	    // Write code here that turns the phrase above into concrete actions
		driver.get("https://acme-test.uipath.com/account/login");
	}

	@Given("Enter the username as (.*)")
	public void enterTheUsername(String uname) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("email").sendKeys(uname);
	}

	@Given("Enter the password as (.*)")
	public void enterThePassword(String pwd) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("password").sendKeys(pwd);
	}

	@Given("Click on Login")
	public void clickOnLogin() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("buttonLogin").click();
	}

	@Given("Click on Search Vendor")
	public void clickOnSearchVendor() {
	    // Write code here that turns the phrase above into concrete actions
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByXPath("//i[@class='fa fa-truck']")).pause(3000)
		.click(driver.findElementByXPath("//i[@class='fa fa-truck']/parent::button/following::ul[1]/li[1]")).perform();
	}

	@Given("Enter TaxID as (.*)")
	public void enterTaxID(String ID) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("vendorTaxID").sendKeys(ID);
	}

	@Given("Click on Search Button")
	public void clickOnSearchButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("buttonSearch").click();
	}

	@When("List is displayed")
	public void listIsDisplayed() {
	    // Write code here that turns the phrase above into concrete actions
		if(!(driver.findElementByXPath("//div[text()='Search Results']/parent::div/div/p").getText()).contains("No vendor")) 
			System.out.println("Vendor ID is available");
	    
	}

	@Then("Get the Vendor Name")
	public void getTheVendorName() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Vendor name of the provided ID is " + driver.findElementByXPath("//div[@class='main-container']/div/table//tr[2]/td[1]").getText());
	}



}
